import { AppDataSource } from "./data-source"
import { Product } from "./entity/Products"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Product)
    // const product = new Product()
    // product.name = "ข้าวหน้าเป็ด";
    // product.price = 50;
    // await productRepository.save(product)
    // console.log("Saved a new user with id: " + product.id)

    //console.log("Loading product from the database...")
    const products = await productRepository.find()
    console.log("Loaded product: ", products)

    const updatedProduct = await productRepository.findOneBy({id: 1})
    console.log(updatedProduct)
    updatedProduct.price = 60
    await productRepository.save(updatedProduct)

}).catch(error => console.log(error))
